# Usability testing in Debian

We use this repo to collaborate on the planning, execution and result
consolidation of usability tests in Debian.

## The Debian installation process

The first phase of the project aims to tackle the Debian installation process.
Not only the installer per se, but the whole process of finding the iso,
verifying it, and finally installing the system.

We would like to investigate how easily users can find their way into the
installation process, and potentially uncover issues that might prevent users
from using Debian.

Instructions to [testers](instructions/instruction-testers.md) and
[observers](instruction-observers.md) provide more details about the setup and
unfolding of tests.  

## Timeline

* Pilot run: August 22nd, 2023
* First phase: August 24th - September 1st, 2023

## Acknowledgement

The main computer used for testing installations was funded by the Debian
Project. Thanks to all donors for [supporting Debian](https://www.debian.org/donations).

## Authors

This study is being conducted by Tassia Camoes-Araujo, Anthony Nadeau and
Giuliana Bouzon.

## Contributing

Comments, suggestions and merge requests are more than welcome! You can reach us
through email, IRC #usability-testing (OFTC server) or through
[Discord](https://discord.gg/rBKb9JRQGD).

We are also open to organizing usability tests in other areas of Debian or FLOSS
in general.


## Previous initiatives and references

* [GNOME and Debian usability testing, May 2017](https://people.debian.org/~intrigeri/blog/posts/GNOME_and_Debian_usability_testing_201705/)
* [GNOME Usability Test Results, Part 1](https://renatagegaj.wordpress.com/2016/08/23/gnome-usability-test-results-part-1/) and [Part 2](https://renatagegaj.wordpress.com/2016/08/26/gnome-usability-test-results-part-2/), by Renata Gegaj
* [It's about the User: Applying Usability in Open-Source Software
](https://www.linuxjournal.com/content/its-about-user-applying-usability-open-source-software), by Jim Hall
* [State Of Linux Usability 2020](https://lawzava.com/blog/state-of-linux-usability-2020/), Law Zava
* [Usability testing: how do we design effective tasks](https://ubuntu.com/blog/usability-testing-how-do-we-design-effective-tasks),
by Canonical
* [Usability Testing & Evaluation of Chores in GNU/Linux for Novice](http://www.diva-portal.org/smash/get/diva2:832464/FULLTEXT01.pdf), by Muhammad Hamid Fayyaz & Usman Idrees
* [Usability Testing Artifacts](https://github.com/clarissalimab/ux), by Clarissa Lima
* [Usability](https://wiki.debian.org/Usability), article on Debian Wiki
