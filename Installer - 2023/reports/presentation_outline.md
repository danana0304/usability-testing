# Debian Installer Usability Presentation

## Outline

- Background, rationale & object of research
- Tests setup & preparation
- Study results
- Comparison with other installers
- Suggestions for improvements
- Final words & thanks

## Background, rationale & object of research

- Usability = how easy user interfaces are to use
- In our case, how easily and clearly it is for users to install Debian
  - standard installer, **netinst ISO**
- How can the Debian installer and/or documentation be improved to give users a better experience?
- Testers were mainly new users with little technical experience
- We wanted to see how they would get by with little to no information and relying only on the provided documentation
  - this might give us hints of what could be improved in the installer/documentation

## Tests

### Our Setup

- 10 Volunteer testers: 1 for the pilot test, 9 to test for real
- Mainly students in the Computer Science Technology program at Vanier College (Montreal, Canada)
- Most of which had little to no technical experience
- 2-hour slots allocated per tester
- Equipment:
  - usb key 32 gb
  - dual-boot windows and debian laptop for part 1 (research)
  - lenovo thinkpad for the installation itself
  - WiFi router
- Testes performed in a room at Vanier college 
![Testing Room](/images/testing_room.jpg)

### Preparation

- Observer greeted the tester and handed a [consent form](/forms/consent-form.md)
- Tester read and signed the form 
- Observer distributed copies of [demographics form](/forms/demographics-form.md) and [simplified task list](/installation/simplified-task-list.md)
- Tester could choose the OS for the "research" laptop
- Observer explaned study goals and what the test was about
- Observer distributed a copy of our [instructions for testers](/instructions/instruction-testers.md).

### Test run

- We started a timer and let them read the simplified task list and make their way through it
- We sat next to them and observed what they were doing
- We also asked questions about it to both understand their train of thought but to maybe help *them* understand what they were doing better
- When needed, we had prepared 3 level of helpers: explanations, hints and tutorials that was open on their research laptop and collected in a [helper table](/installation/README.md), for easy access
- The goal was to let them be as autonomous as possible and if needed, we would show them the above documentation that we had prepared.
- We also allowed the use of Internet searches and watching online video tutorials.
- We took notes and observed their struggle points and wrote down any feedback they had during the test.

## Results

### Time

- Average time taken to complete the test was 1h42min
- Half the time of the task was needed only for part 1

### Testers & Demographics

We tested 9 people, 8 of which were college students and 1 of which was a designer. <br>
![Graph - Occupation](/reports/demographics-data/Demo_Occupation.png)

Most of our testers were 18 and 19 years old with the exception of one, being 40 years old. <br>
![Graph - Age](/reports/demographics-data/Demo_Ages.png)

67% of our testers were male and 33% female. <br>
![Graph - Gender](/reports/demographics-data/Demo_Gender.png)

We had varying native languages, but most were English. <br>
![Graph - Language](/reports/demographics-data/Demo_Language.png)

Almost all of our testers used Windows on their personal computers, with the exception one 1 who used MacOS. Furthermore, most of our testers had no experience with Linux. <br>
![Graph - Experience](/reports/demographics-data/Demo_Linux-Experience.png)

Most of our testers had little to no prior experience with Linux and the installation of operating system.
![Graph - Experience](/reports/demographics-data/Demo_OS-Experience.png)

### Heatmap

Here is the [simplified task list](/installation/simplified-task-list.md) for reference.

Heatmap <br>
![Heatmap](/reports/HeatMapComplete.png)

## Comparison

### Tails

#### Documentation

The documentation on the Tails project is very well organized and simple to understand. 

In their download verification (checksum, signature), all the users have to do is choose their downloaded file and Tails will verify it for them, no need to download external software or be knowledgeable about checksums and the command line.

Here's what their download verification page looks like <br>
![Download Verification](/images/tails/tails_verify.png)

What happens after verification (if the file as good) <br>
![Download Verify](/images/tails/tails_verify3.png)

Here's what happens when it's a bad download (or wrong file) <br>
![Bad Download](/images/tails/tails_badverify.png)

#### Installation

Tails' installation process is also very simple. Their documentation on how to install from Windows has visual aids and explanations <br>
![Windows Tails Install](/images/tails/tails_win_install.png)

After booting into the image from the USB key, we have a very simple installation process, offering more advanced options for users who might need it but keeping it simple nonetheless <br>
![Tails Installation](/images/tails/tails.png)

Overall, **Tails has excellent documentation and a very simple installer** as demonstrated above that we could use as examples in order to improve our own. 


### Ubuntu

#### Documentation

Much like Tails, Ubuntu also provides an easy way of verifying the download of the ubuntu image: but through a tutorial on their website.
![Ubuntu Download Verification](../images/Ubuntu/ubuntu_verify.png)

They even offer a tutorial of wsl2 on Windows which encourages the use of Linux Tools!
![Ubuntu WSL2](../images/Ubuntu/wsl_tutorial.png)

They also offer a tutorial on how to create a bootable USB stick
![Ubuntu Bootable Media Tutorial](../images/Ubuntu/ubuntu_bootable_tutorial.png)

Unlike Tails, Ubuntu does not offer a tool to check the download themselves, they provide instead extensive documentation with easy tutorials, meant for any and all types of users.

On another note, one thing Ubuntu offers that is different from both Tails and Debian is their project UI. It is very easy to navigate and very easy to visualize the important information. This is a very nice addition for those of us who are more visual and tend to skim through documentation or only look for things that are emphasized.
![Ubuntu UI](../images/Ubuntu/ubuntu_ui.png)

#### Installation

Ubuntu's installation process is also very simple and asks few questions but allows for more advanced configuration. Basically, a user must enter language, keyboard, installation type (which covers disk partitioning), location and then user setup (username, computer name, password) <br>
![Ubuntu Disk Partitioning](/images/Ubuntu/ubuntu_disk.png)
![Ubuntu User Setup](/images/Ubuntu/ubuntu_user_creation.png)

Overall, **Ubuntu's installation process requires few information from the user** to setup the environment, offers a **nice UI and lots of online tutorials** in their project website in case the user needs help. They also have more **advanced features for the more expert users**.

### Fedora

#### Documentation

At first glance, something Fedora and Ubuntu have in common is their UI. It is easy to navigate and to find the important things. That makes it more attractive to new users.

To start, Fedora also has a tutorial to verify the download, but it is very short and only tails to Linux users (commands) 
![Fedora Verify](../images/Fedora/fedora_verify.png)

However, Fedora doesn't have much more in terms of documentation. It is very limited and a lot of it contains mostly walls of text.

#### Installation

Fedora's installation process follows suit with Tails and Ubuntu. It is very simple and asks few questions: language, keyboard, time & date and disk. 

![Fedora Installation - Language](/images/Fedora/fedora_install1.png)

It sets time & date based on location and sets the default option for the disk partitioning (free space on main disk). By clicking on the different options, we're able to change these options and add more customization if we wish to but these options are good enough for new users.
![Fedora Installation - Localization & System](/images/Fedora/fedora_install2.png)

I was also able to use Firefox and other tools while the system was installing.
![Fedora Live](/images/Fedora/fedora_multiuse.png)

Overall, although **Fedora's documentation doesn't seem very helpful to new users**, the installation system is. It's very **fast and simple to use and being able to use the system while it's installing is also a nice feature**. One small but cool feature: the icons are super beautiful!


### Calamares

Another good example of a simple installation process catering to new users is the Calamares installer. It asks **very few questions and visualizes some sections in an easier way to understand**. 

Here shows the different sections of the installation (location, keyboard, partitions and users) <br>
![Different Sections of Installation](/images/Calamares/cal_install1.png)

This shows an easier visualization of the partitioning and what each one means <br>
![Partitioning](/images/Calamares/cal_install4.png)

And lastly, this image shows another way of asking for account and user setup <br>
![User Accounts](/images/Calamares/cal_install5.png)

## Discussion & Suggestions

**These conclusions and subsequent suggestions were inspired by our observations during the test, and we did not account for the feasibility or complexity of implementing them**

- Part 1:
  - most testers assumed the iso was automatically downloaded after pressing the big download button on debian.org
  - none of the testers were able to complete tasks 1.2 and 1.3 easily (most needed tutorials and those that didn't took significantly longer)
  - it seems that the documentation provided may not be sufficiently clear or complete with information needed to be able to install the system, start to finish
  - documentation mentions checksums and signature and other more technical concepts that are not common knowledge 
  - Google tutorials were often used 

- Part 2:
  - the difference between "graphical install" and "install" was not clear
  - the difference between wep and wpa was unclear 
  - confusion regarding what to put for hostname, domain name, network mirror and HTTP proxy

- Part 3:
  - testers had difficulty finding the applications button on the Gnome desktop environment

Overall, we discovered many improvements that could be done to both the installer and the documentation associated. 

### Website & Documentation

**Main point: we make sure we're accomodating to people of all backgrounds!**

By comparing our documentation and overall website organization to other distros like Ubuntu and Tails, we noticed some changes that we could make to improve overall usability:

- Modify wording and explanations to make it more easily understood by people with no technical background (take the explanation on verifying the authenticity of Debian images for example) or include links to explanations of *signed checksum files* as an example.
- Section documentation in parts easily visualizable by the user. For example, the Tails installation process: it contains images and the different steps and then an explanation and different tools for each one. The Ubuntu one is also a very good example. It provides tutorials or links to tools to be used for the different steps (like BalenaEtcher for flashing a USB key, maybe include a direct download link to BalenaEtcher).
- If we ask users to verify the download with the checksum, specify that the checksum provided used *SHA512* algorithm
- Rename files **SHA512SUMS** and **Signature** to something more consistent, like **Checksum** and **Signature** or **SHA512SUMS** and **SHA512SIGNATURE**
- Adding links to tools and/or writing tutorials for users to get through installation
- Account for users of different operating systems like Windows, MacOS and Linux

Although there *is* an installation guide providing some of this information, it is all condensed and full of text which may make it hard to navigate to some people. Maybe adding some images and links, emphasizing the more important parts and sectionizing it would be helpful.

Also, revamping the website and documentation would also add a nice touch as it would be more attractive to users while taking into consideration newer website development conventions (like the navigation bar that is there no matter what page the user is in) 

### Installer

By comparing the standart Debian installer (netinst) to different installers like Calamares, we noticed some changes and discussion points that could bring about valuable changes to the current installer:

- Section the installation into a "simple installer" like Calamares asking only required questions (language, location, keyboard, user creation and partitioning) and an "Advanced installer" that could offer more configuration to expert users or to users wanting more customization.
- Most people tend to skim through text, so a small but significant change would be to make the important text in bold.
- Maybe consider changing the installer names to: "Graphical Installer" and "Legacy Installer" to make it more obvious that the "Install" option is more for a headless system or a system with no graphics.
- In the desktop environment choice, consider adding "previews" of the different desktop environments to help new users see the difference and make a better informed choice.

### Desktop Environment (Gnome)

A personal suggestion that I think would make for a very cool addition:

- In the initial setup menu, consider adding a screen at the end about themes to help users familiarize themselves with the different options for themes and be able to customize their system or even a link to it (https://wiki.debian.org/DebianArt/Themes) <br>
![Initial Setup Menu Gnome](/images/Gnome/initialsetupmenu_gnome.png)



## Thank You!

thank you to our pilot tester, Kerian Loerick, Jayda, Zlatin and all our other testers<br>
thank you to Debian contributors/sponsors for giving us the budget to acquire the material needed (laptop for installation) <br>
thank you to Vanier college for the physical and network space <br>
thank you to the Computer Science department at Vanier for the router <br>
thank you to the observers and contributors <br>
and thank you for listening!




