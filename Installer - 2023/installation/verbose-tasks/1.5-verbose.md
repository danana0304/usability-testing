### Boot from the USB key you have just flashed

To boot into something means to load an operating system into the computer's
main memory or RAM. Once the operating system is loaded (for example, on a
Windows PC), we will see the initial Windows screen. In our case, we want to
boot and start the Debian installer that was saved in the USB. Look for clues on the computer's boot menu.

Hint: [1.5-hint](/installation/hints/1.5-hint.md)
