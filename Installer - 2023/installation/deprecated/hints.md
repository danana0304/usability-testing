# Hints for Tasks for Installation of Debian 12 (Orange)

## Getting the Debian 12 image onto a USB key

1. The download link can be found [here](https://www.debian.org/download)

2. You can find ways to get the checksum of a file in Windows [here](https://answers.microsoft.com/en-us/windows/forum/all/finding-checksum-values-in-windows-10/dbc3c569-4b5a-4967-8810-c25255cdc1fd)

3. You can use GnuPG

4. You can use BalenaEtcher for this

5. If the device has multiple boot devices, you can press **F12** to open the boot menu.

## Starting the Installation 

1. Choose "Graphical Install"

2. examples: "English" or "French"

3. examples: "Canada" or "United States"

4. select the keyboard layout your device uses, example: ENG, US for the american keyboard layout.

5. Look for the interface with the keyword "Wireless" since we're using WiFi.
   
6. n/a
   
7. n/a
   
8. You can pick from different options:
   - Guided partition - Use entire disk: the system will partition the *entire* disk for you.
   - Guided partition - use entire disk and set up LVM: the system will set up Logical Volume Management *and* partition the entire disk for you. LVM provides flexibility in managing storage volumes.
   - Guided partition - use entire disk and set up encrypted LVM: the system will setup encrypted LVM as an extra layer of security in addition to partitioning the entire disk.
   - Manual partitioning: you can customize and define the partitions according to your own requirements.
   
9. n/a

10. It is recommended to choose "Yes" to get the most up-to-date packages.

11. n/a

12. n/a

13. If you have more than one device (example, the main disk and a USB key), make sure to choose the one where you're currently installing this system into (this would have been selected during disk partitioning).

After this, the installation will be complete and you'll need to reboot the system and remove the USB key.

## After the Installation

1. username and password defined during user creation.

2. Change your wallpaper
   This can be changed through the "Settings".


**Well done, you've completed all the tasks!**