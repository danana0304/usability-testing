# Usability testing in Debian

We use this repo to collaborate on the planning, execution and result
consolidation of usability tests in Debian.

## Phases

We are planning on having multiple phases of usability testing in Debian, to cover different aspects of Debian (installer, website, etc). Each phase has a dedicated folder.

## Contributing

Comments, suggestions and merge requests are more than welcome! You can reach us
through email, IRC #usability-testing (OFTC server) or through
[Discord](https://discord.gg/rBKb9JRQGD).

We are also open to organizing usability tests in other areas of Debian or FLOSS
in general.