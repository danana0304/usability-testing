# Statistical analysis - Brainstorming

## Resources
- Article on [hypothesis testing](https://www.scribbr.com/statistics/hypothesis-testing/) (sent by Tassia on Discord)
  - Remark: Hypothesis testing seems to be the best approach for our study
- Introduction to [statistical significance](https://www.scribbr.com/statistics/statistical-significance/): how to decide to reject or accept the null hypothesis
  - Remark: Depending on the size of our dataset, we might need to set a significance level higher than 5% (a S.L. of 5% means that we can reject the null hypothesis *if and only if* the results have less than 5% chance of occuring from random noise)
- Article on different [statistical tests](https://www.scribbr.com/statistics/statistical-tests/) and how to choose the best one

## Variables we can consider
- The order in which a participant uses each Debian installer [binary variable]

- The difficulty rating given to each installation process (as a whole, or as a sum of parts) [discrete variable]
  - Note: We would have to establish a scale of difficulty for reliable results. Some more objective ways of quantifying difficulty are also possible (such as time taken to install, or amount of help needed: see below)

- The total time taken to install the OS with each installer [continuous variable]
  - Note: To compare apples with apples, we first have to make sure the processes are comparable
  - For example, if we provide the USB key with the Calamares installer, we would *not* include the time taken to flash the "standard" installer on a USB key

- The amount of help needed [discrete variable]
  - There are 3 levels of help available for each task. In order from least to most helpful: requesting a verbose version of the task, requesting a hint, and requesting a tutorial to watch or read.
  - We could assign a value to each of them (for example, 1 for verbose task, 3 for hint, 5 for tutorial) and do a sum of these values to get a numeric total representing how much help a participant needed.

- The participant's technical abilities [multiple variables]
  - This can be a "sum" of many variables, such as (from most relevant to least relevant): experience with operating systems, self-reported ability with computers...
  - We can also consider each of these variables separately to see which ones have an effect on different factors

## What we wish to achieve
Possible hypotheses to test: these are stated as [null hypotheses](https://en.wikipedia.org/wiki/Null_hypothesis)

Here, the term "**overall difficulty**" is a combination of 3 variables: the difficulty rating on a scale of 1-5 given by the participant, the total time required to install the OS, and the amount of help needed to install the OS

So, when testing the relationship between a variable **x** and the **overall difficulty**, we will look at the relationship between **x** and each of theses 3 variables *separately*.

- There is no relation between the **order** in which a participant uses an installer (first or second) and the **overall difficulty** to install the system using that installer
- There is no relation between **the installer used** (standard vs. Calamares) and the **overall difficulty** to install the system using that installer
- There is no relation between a person's **technical abilities** (experience with operating systems, other technical experience such as programming, field of study...) and the **overall difficulty** to install the system *using the Calamares installer*
- Same as above, but using the standard Debian installer

## Means to achieve these results
*TODO* Which statistical tests will we use to compare data? What [significance level](https://en.wikipedia.org/wiki/Statistical_significance) will we take?

*TODO* run an example analysis using a few made-up datasets

*TODO* get feedback on our methodology from an expert