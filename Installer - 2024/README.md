# Usability testing in Debian - 2024 -> TO BE UPDATED ***

We use this repo to collaborate on the planning, execution and result
consolidation of usability tests in Debian.

## The Debian installation process

The second phase of the project aims to understand if there is a correlation between 
a user's perspective on Debian and its installation process and which installer is used (PLACEHOLDER)

We would like to examine the process of installing Debian using the standard netinst installer and the Calamares installer and analyze the difficulty level for both from the perspective of new users to Linux.

Instructions to [testers](instructions/instruction-testers.md) and
[observers](instruction-observers.md) provide more details about the setup and
unfolding of tests.  

## Timeline

* Pilot run: TBD
* First phase: TBD

## Acknowledgement

The computers used for testing installations was funded by the Debian
Project and by the Eastern Bloc organization in Montreal. Thanks to all donors for [supporting Debian](https://www.debian.org/donations).

## Authors

This study is being conducted by Tassia Camoes-Araujo, Anthony Nadeau, 
Giuliana Bouzon and Justin Bax.

## Contributing

Comments, suggestions and merge requests are more than welcome! You can reach us
through email, IRC #usability-testing (OFTC server) or through
[Discord](https://discord.gg/rBKb9JRQGD).

We are also open to organizing usability tests in other areas of Debian or FLOSS
in general.


## Previous initiatives and references

* TBD
