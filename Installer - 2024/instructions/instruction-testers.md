# Instructions for testers -> TO BE UPDATED ***

First of all, thank you for participating in this study!

Our goal is to find out how easy and clear it is for users with a variety of
experiences to install Debian.

## Usability testing intro

"Usability" in our context refers to how easily users can learn and start
installing Debian GNU/Linux. The target of our test is the Debian installer and
corresponding documentation, including the website. We aim to investigate how
those tools are well suited to our users' needs. 

## Rationale

The main goal of this study is to be able to establish in what ways the Debian
installer and/or documentation could be improved to give users a better
experience, while considering a variety of users' background - with different
knowledge and exposure to UNIX systems.

We plan to run this test with ~10 people in order to obtain meaningful results.
We also plan on doing a compare and contrast with other popular distributions of
Linux, such as Fedora, Linux Mint and Ubuntu to find ways in which we could
improve.

## Setup

We will use a USB key [1], a laptop for the installation [2], and another laptop to
prepare the bootable media and to look up documentation as needed [3]. All of
which will be provided by us. All you need is to show up :)

[1] USB key, 32GB (thanks to Giuliana Bouzon).

[2] Lenovo ThinkPad X380 Yoga with 16GB of RAM, Intel processor i5-8350U, 512GB
SSD and Integrated graphics (thanks to the Debian Project donors).

[3] HP Omen 15 with 16GB of RAM, Intel processor i7-10750H, 1TB NVME storage and a
  NVIDIA Geforce RTX 3060 GPU (thanks to Giuliana Bouzon).

Additionally, participants are allowed to bring their own hard drives, if they
wish to take home the installed system (optional).

## Execution

We are going to observe you while you perform tasks, however, by no means we are
testing your performance. We must make notes about your (potential) struggles in
order to improve Debian for future users. Whenever possible, **please
verbalize** what you are thinking, so that we can follow your chain of thoughts
and make useful notes.

## Follow up

The results from these tests will be compiled together in order to (potentially)
uncover common pain points, which might give hints about what needs to be
improved. We will categorize those points and see if they are related to
the documentation and/or the installer itself.

Then we plan to compare with different distributions, how they tackle those points.

Once all this information is compiled and analyzed, we will be able to give
suggestions to Debian developers as to what we think could be improved.
