# Instructions for observers

We will start with a pilot test ("test" tester) to ensure all our steps are
clear and concise. Please update this document if you notice anything that must
be improved.

## Test setup

Before starting a test:

- Format USB key
- Delete all browser history
- Uninstall all apps installed (BalenaEtcher)
- Delete any files downloaded 
- use command <code>Clear-History</code> on Powershell to delete command history

During the test:

- Start by handing testers a [consent form](/forms/consent-form.md).
- Then ask to complete a short [demographic questionnaire](/forms/demographics-form.md).
- Read out loud the [testers
  instructions](/documentation/instruction-testers.md) and explain to them what
our goals are.
- Ask if they prefer Windows or Linux in the auxiliary laptop, and boot accordingly.
- Finally, hand out the [simplified task
  list](/installation/simplified-task-list.md) and observe.

**Don't forget to keep track of the time. Start a timer and take notes!**

## What to note down during a test session

- The time the test started, and the time each task started to be worked on
- Any time the tester express some difficulty, read out loud repeatedly in order
  to understand, etc, make a note for the corresponding task
- Make a note of concepts that we not clear to the tester
- For each task, if they need more information, provide them with the corresponding
  verbose explanation.
- If they need help to proceed, they should access the corresponding extra hint.
- Finally, if even hints are not enough, they should access the corresponding tutorial,
  and the observer should help them and assure that the next step can be performed.
- Make a note about how much help was needed (if any) in each step.

Follow the [observer report](/installation/observer-report.md) as a task list
for observers to take notes and grade the tester.

## Grade Scheme

- Green (4 - easy): The tester was able to perform the task using only the simplified task list.
- Yellow (3 - somewhat difficult): The tester needed the verbose task list to perform the task.
- Orange (2 - difficult): The tester needed hints from us to perform the task.
- Red (1 - very difficult): The tester needed a step-by-step or a tutorial from us to perform the task.

## Beyond a test session

- Statistical Analysis

- Produce heat maps and observations along with recommendations based on the
struggles our testers experienced in our final report of these tests.
