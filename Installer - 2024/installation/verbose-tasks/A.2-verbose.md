### Head to the Debian project website and download the Calamares image

Head to debian.org/distrib and look for a download button. This will allow you to download the Calamares Gnome live image (.iso file)

Hint:  [A.2-hint](../hints/A.2-hint.md)