### Flash the iso image onto the provided USB key

This step is needed so that we are able to boot into the image through the USB key and start the installation process. Search online for how you can flash USB keys for clues if you don't know how to!

Hint: [A.3-hint](../hints/A.3-hint.md)