### Head to the Debian project website and download the Debian 12 image

Head to debian.org and look for a download button. This will allow you to download the debian 12 image(.iso file).

Hint:  [A.1-hint](../hints/A.1-hint.md)
