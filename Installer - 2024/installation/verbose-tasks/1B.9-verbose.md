### Option to participate in popularity contest

By choosing "Yes", you give consent to sending package statistics to Debian
on a weekly basis. This information helps developers in improving Debian,
for instance giving higher priority for the development of very popular packages.

Hint: [1B.9-hint](../hints/1B.9-hint.md)
