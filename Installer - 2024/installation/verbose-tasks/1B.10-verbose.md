### Select and install software

Only the core components of the system are installed by default. To customize
your environment, you can choose to install one or more predefined collections
of software. This is optional.

Hint: [1B.10-hint](../hints/1B.10-hint.md)

