Say "No" to scan additional media, and "Yes" to select a network mirror. Then
you can safely go with the default mirror selection.
