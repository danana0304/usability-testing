## Demographic form

The purpose of this form is to collect demographic data that may be pertinent to how you perform in the tests. That being said, we'd like to reiterate that **the purpose of this study is to test the system, not you.** This form will be used solely to segment the data we collect.

*To begin please fill out the following*:

**Your age**: ____

**Your gender identity**: 
- [ ] Male
- [ ] Female 
- [ ] Non-Binary
- [ ] Other: ______________

**What is your native language?**:
- [ ] English
- [ ] French
- [ ] Other: ______________

**Your Occupation** (If you're a student, specify in which program): ___________________________

**Proficiency with computers**: __________
- [ ] Beginner (I usually need help)
- [ ] Intermediate (I can find my way around)
- [ ] Advanced (I am tech-savvy)

**Which operating system do you have installed in your personal computer**: ____________________

**Experience in the installation of operating systems**:
- [ ] None 
- [ ] Some (you've done it before)
- [ ] A lot (you're comfortable with it)

**Familiarity with Linux**: 
- [ ] None 
- [ ] Some (you've used it before)
- [ ] A lot (you use it on a regular basis)

