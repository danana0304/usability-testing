## Registration form

**Description**
The F.L.O.S.S Club at Vanier is performing a study on how usable the two main installers are for the GNU/Linux operating system Debian. For any who may not know, F.L.O.S.S stands for Free/Libre Open Source Software, and our club focuses on learning to contribute to open source projects, this time, the project being Debian. The goal of these tests is to see how well individuals of varying backgrounds and interests manage with the current installer, so we can find points that the software can be improved upon. Should you register, you'll come in at a set time, and given a list of instructions, you'll install Debian onto a laptop provided to you using each installer. We'd like to emphasize that the tests are to improve the system, and as such we aren't evaluating the skills of the individual. We will welcome anyone and everyone to participate in the study. As an incentive, we are raffling off a laptop that was donated to us to the people participating in the study.

**Email**:___________________________

**Name**:___________________________

**Please check the boxes of dates you are available (Monday - Friday)**
*Week of May 19th:* [ ] [ ] [ ] [ ] [ ]
*Week of May 26th:* [ ] [ ] [ ] [ ] [ ]
*Week of June 2nd:* [ ] [ ] [ ] [ ] [ ]
