## Feedback form

The purpose of this form is to collect information about the tests conducted and any feedback that might be useful for the analysis.

*To begin please fill out the following*:

**Rate the difficulty level for the Calamares Installer**
- [ ] The steps were fast and simple
- [ ] It was okay
- [ ] I only had some minor problems
- [ ] I was confused some of the time
- [ ] I was confused most of the time

**Rate the difficulty level for the standard Debian 12 installer**
- [ ] The steps were fast and simple
- [ ] It was okay
- [ ] I only had some minor problems
- [ ] I was confused some of the time
- [ ] I was confused most of the time

**Any other feedback and/or comments you'd like to share?**

------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------

**How did you find out about this study?**
- [ ] I saw the posters at Vanier
- [ ] Through a friend
- [ ] Through the email sent to Vanier students
- [ ] Discord server
- [ ] Other: _______________________________________________


